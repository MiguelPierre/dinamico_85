<?php
    
    class Categoria
    {

        
        private $id;
        private $categoria;
        private $cat_ativo;

       
        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id=$value;
        }
        public function getCategoria()
        {
            return $this->id;
        }
        public function setCategoria($value)
        {
            $this->categoria = $value;
        }
        public function getCatAtivo()
        {
            return $this->cat_ativo;
        }
        public function setCatAtivo($value)
        {
            $this->cat_ativo = $value;
        }
        # =========================================================================================
        // INSERINDO AS CATEGORIAS NO BANCO
        # =========================================================================================
        public function inserir($_categoria,$_ativo)
        {
            $sql = new Sql();
            return $sql->select('call sp_categoria_insert(:categoria,:cat)',array(':categoria'=>$_categoria,':cat'=>$_ativo));                
        }
        # =========================================================================================================================
        # LISTANDO AS CATEGORIAS DA CATEGORIA
        # =========================================================================================================================
        public static function listar()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM categoria');
        }
        # =========================================================================================================================
        # METODO DE CONSULTA POR ID
        # =========================================================================================================================
        public function consultarId($_id)
        {
            $sql = new Sql();
            $sql->select('select * from categoria where id_categoria = :id',array(':id'=>$_id));
        }
        # =========================================================================================================================
        # METODO DE CONSULTA
        # =========================================================================================================================
        public function searchByName($_categoria)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM categoria where categoria LIKE :categoria',array(':categoria'=>'%'.$_categoria.'%'));
        }
        # =========================================================================================================================
        # MEDOTO UPDATE
        # =========================================================================================================================
        public function update($_id,$_categoria,$_cat_ativo)
        {
            $sql = new Sql();
            $sql->query('UPDATE categoria set categoria = :categoria, cat_ativo = :ativo where id_categoria = :id',
            array(':id'=>$_id,':categoria'=>$_categoria,':ativo'=>$_cat_ativo));
        }
        # =========================================================================================================================
        # METODO DE DELET
        # =========================================================================================================================
        public function delete($_id)
        {
            $sql = new Sql();
            $sql->query('delete from categoria where id_categoria = :id',array(':id'=>$_id));
        }
        # =========================================================================================================================
        #Define o valor que o banco de dados retornar para os atributos
        # =========================================================================================================================
        public function setData($data)
        {
            $this->setId($data['id_categoria']);
            $this->setCategoria($data['categoria']);
            $this->setCatAtivo($data['cat_ativo']);
        }
        public function __construct($_id='',$_categoria='',$_ativo='')
        {
            $this->id = $_id;
            $this->categoria = $_categoria;
            $this->cat_ativo = $_ativo;
        }
    }
?>