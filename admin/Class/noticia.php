<?php
    class Noticia
    {
        private $id;
        private $id_categoria;
        private $titulo;
        private $img;
        private $visita;
        private $data;
        private $ativo;
        private $noticia;
// ==========================================================================================================================================
// ================================================================== METODOS 
        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id = $value;
        }
// ==========================================================================================================================================
// ================================================================== METODOS ACESSO AO ID
        public function getIdCategoria()
        {
            return $this->id_categoria;
        }        
        public function setIdCategoria($value)
        {
            $this->id_categoria = $value;
        }
// ==========================================================================================================================================
// ================================================================== METODO  DE INSERÇÃO PARA O TITULO
        public function getTitulo()
        {
            return $this->titulo;
        }
        public function setTitulo($value)
        {
            $this->titulo = $value;
        }
 // ==========================================================================================================================================
// ===========================================================================================================================================
        public function getImg()
        {
            return $this->img;
        }
        public function setImg($value)
        {
            $this->img = $value;
        }
 // ==========================================================================================================================================
// ===========================================================================================================================================
        public function getList()
        {
            $sql = new Sql();
            return $sql->select('select * from noticias');
        }

        public function getVisita()
        {
            return $this->visita;
        }
        public function setVisita($value)
        {
            $this->visita = $value;
        }
   
        public function getData()
        {
            return $this->data;
        }
        public function setData($value)
        {
            $this->data = $value;
        }
        //Métodos de acesso a  getativo
        public function getAtivo()
        {
            return $this->ativo;
        }
        public function setAtivo($value)
        {
            $this->ativo = $value;
        }
        //Métodos de acesso a noticia
        public function getNoticia()
        {
            return $this->noticia;
        }
        public function setNoticia($value)
        {
            $this->noticia = $value;
        }

        // ==========================================================================================================================
        // ================================================================================================================== METODO
        // ==========================================================================================================================
        //Inserindo noticia
        public function inserirNoticia($_idCategoria,$_titulo,$_img,$_data,$_ativo,$_noticia)
        {
            $sql = new Sql();
            $sql->query('INSERT INTO noticias (id_categoria,titulo_noticia,img_noticia,visita_noticia,data_noticia,noticia_ativo,noticia)
            values (:categoria,:titulo,:img,0,:_data,:ativo,:noticia)',
            array(':categoria'=>$_idCategoria,  ':titulo'=>$_titulo, ':img'=>$_img, ':_data'=>$_data, ':ativo'=>$_ativo, ':noticia'=>$_noticia));
        }
        //Atualizando noticia
        public function updateNoticia($_id,$_idCategoria,$_titulo,$_img,$_data,$_ativo,$_noticia)
        {
            $sql = new Sql();
            $sql->query('UPDATE noticias set id_categoria = :categoria,titulo_noticia = :titulo,img_noticia = :img,
            visita_noticia = :visita,data_noticia :_data,noticia_ativo = :ativo,noticia = :noticia where id_noticia = :id',            
            array(':categoria'=>$_idCategoria, ':titulo'=>$_titulo, ':img'=>$_img, ':_data'=>$_data, ':ativo'=>$_ativo, ':noticia'=>$_noticia, ':id'=>$_id));
        }
        //Delete noticia
        public function deleteNoticia($_id)
        {
            $sql = new Sql();
            $sql->query('DELETE from noticias where id_noticia = :id',array(':id'=>$_id));
        }
        //Listando noticia
        public function listarNoticias()
        {
            $sql = new Sql();
            return $sql->select('select * from noticias');
        }
        //Consultando noticia por id
        public function consultarId($_id)
        {
            $sql = new Sql();
            return $sql->select('select * from noticias where id = :id',array(':id'=>$_id));
        }
        //consultando por Titulo
        public function consultarTitulo($_titulo)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM noticias where titulo_noticia LIKE :titulo',array(':titulo'=>'%'.$_titulo.'%'));
        }    
        //Método construtor
        public function __construct($_id='',$_idCategoria='',$_titulo='',$_img='',$_data='',$_ativo='',$_noticia='')
        {
            $this->id = $_id;
            $this->id_categoria = $_idCategoria;
            $this->titulo = $_titulo;
            $this->img = $_img;
            $this->data = $_data;
            $this->ativo = $_ativo;
            $this->noticia = $_noticia;
        }   
        
        public function updateVisita($id)
        {
            $sql = new Sql();
            $sql->query("UPDATE noticias SET visitas_noticia = visita_noticia +1 WHERE id_noticia = :id", 
            array( ":id"=>$id));
        }
    }
?>