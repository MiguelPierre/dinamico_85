<?php
    class Usuario
    {
        #Definindo atributos
        private $id;
        private $nome;
        private $email;
        private $senha;
        private $foto;
        
        #Definindo métodos de acesso aos atributos
        public function setId($value)
        {
            $this->id = $value;
        }
        public function getId()
        {
            return $this->id;
        }
        public function setNome($value)
        {
            $this->nome = $value;
        }
        public function getNome()
        {
            return $this->nome;
        }
        public function setEmail($value)
        {
            $this->email = $value;
        }
        public function getEmail()
        {
            return $this->email;
        }
        public function setSenha($value)
        {
            $this->senha = $value;
        }
        public function getSenha()
        {
            return $this->senha;
        }
        public function setFoto($value)
        {
            $this->foto = $value;
        }
        public function getFoto()
        {
            return $this->foto;
        }
        #Métodos

        #Inserindo Usuario ========================================================================================================================
        public function insertUser($_nome,$_email,$_foto)
        {
            $sql = new Sql();
            $result = $sql->select('call sp_usuario_insert (:nome,:email,:senha,:foto)',array(':nome'=>$_nome,':email'=>$_email,':foto'=>$_foto));
            if(count($result)>0)
            {
                $this->setData($result[0]);
            }
        }
        #Alterar Usuario
        public function updateUser($_id,$_nome,$_email,$_foto)
        {
            $sql = new Sql();
            $sql->query('UPDATE usuario SET nome = :nome, email = :email, foto = :foto WHERE id = :id',
            array(  ':nome'=>$_nome,
                    ':email'=>$_email,
                    ':foto'=>$_foto,
                    ':id'=>$_id));
        }
        #Excluir Usuario ==================CONCERTAR========================================================================================
        public function deleteUser($_id)
        {
            $sql = new Sql();
            $sql->query('delete from usuario where id = :id',array(':id'=>$_id));
        }
        #Listando usuarios ===================================PRONTO===================================================
        public function listarUsuarios()
        {
            $sql = new Sql();
            return $sql->select('select * from usuario');            
        }
        #Consultar usuario pelo id ================= PRONTO======================================================>
        public function consultarUserId($_id)
        {
            $sql = new Sql();
            return $sql->select('select * from usuario where id = :id',array(':id'=>$_id));   
        }
        #Definindo dados do banco de dados aos atributos
        public function setData($dados)
        {
            $this->id = $dados['id'];
            $this->nome = $dados['nome'];
            $this->email = $dados['email'];
            $this->foto = $dados['foto'];
        }
        public function efetuarLogin($_email, $_senha) #METODO PARA EFETUAR LOGIN /CONSULTADO NO BANCO/ ========>
        {
            $sql = new Sql();
            $senha_cript = md5($_senha);
            $results = $sql->select("SELECT * FROM usuario WHERE email = :email AND senha = :senha",
            array(':email'=>$_email,":senha"=>$senha_cript));
            if (count($results)>0) 
            {
                $this->setData($results[0]);
            }
        }
        
    }
?>