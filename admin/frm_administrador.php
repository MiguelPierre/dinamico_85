<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Administrador</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="formulario-menor">
        <form action="op_administrador.php" method="POST">
            <fieldset>
                <input type="hidden" id="id" name="id">
                
                <!-- Input NOME -->
                <label for="">NOME</label>
                <input type="text" name="nome" required>
                <br>
                <!-- Input EMAIL -->
                <label for="">EMAIL</label>
                <input type="text" name="email" required>
                <br>
                <!-- Input LOGIN -->
                <label for="">LOGIN</label>
                <input type="text" name="login" required>
                <br>
                <!-- Input SENHA -->
                <label for="">SENHA</label>
                <input type="password" name="senha" required>
                <br>
                <!-- Input confirmar senha -->
                <label for="">CONFIRMAR SENHA</label>
                <input type="password" name="confirmar-senha" required>
                <br>
                <br>
                <!-- Botão Entrar -->
                <input type="submit" name="cadastro" value="cadastrar  Administrador">
                
            </fieldset>
        </form>
    </div>
    
</body>
</html>