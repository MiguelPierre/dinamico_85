<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">    
    <title>Cadastro de Usuario</title>
</head>
<body>        
    <div id="box-cadastro">
        <div id="formulario-menor">            
            <form action="op_usuario.php" method="post" enctype="multipart/form-data" name="cadastro_form">
                <legend>Novo Usuario</legend>
                <fieldset>
                    Nome: <br>
                    <input type="text" name="nome" class="form-control"><br><br>
                    Email: <br>
                    <input type="email" name="email" class="form-control"><br><br>
                    Foto de exibição: <br>
                    <input type="file" name="foto" class="form-control"><br><br>
                    <input type="submit" value="Inserir Usuario"  name="cadastro_usuario" class="btn btn-succes">
                </fieldset>
            </form> 
        </div>
    </div>
</body>
</html>