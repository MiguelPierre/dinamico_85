<?php
    require_once('../config.php');            
    // $noticia = new Noticia();
    $noti = new Noticia();
    if(count($nots = $noti->listarNoticias()) > 0)
    {
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Lista Noticias</title>
</head>
<body>
    <table width='100%' border="" cellpadding="0" cellspacing="2" bgcolor="">
        <tr bgcolor="#968a89" align="center">
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">ID</font></th>
            <th width="15%" height="2" align="rigth"><font size="2" color="#fff">Categoria</font></th>
            <th width="20%" height="2" align="rigth"><font size="2" color="#fff">Titulo</font></th>
            <th width="20%" height="2" align="rigth"><font size="2" color="#fff">Imagem</font></th>
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">Visitas</font></th>
            <th width="15%" height="2" align="rigth"><font size="2" color="#fff">Data</font></th>
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">Ativo</font></th>
            <th width="20%" height="2" align="rigth"><font size="2" color="#fff">Noticia</font></th>
            <th colspan="2" align="center"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php            
            foreach($nots as $not)
            {
        ?>
        <tr>
            <td><?php echo $not['id_noticia']?></td>
            <td><?php echo $not['id_categoria']?></td>
            <td><?php echo $not['titulo_noticia']?></td>
            <td><?php echo $not['img_noticia']?></td>
            <td><?php echo $not['visita_noticia']?></td>
            <td><?php echo $not['data_noticia']?></td>
            <td><?php echo $not['noticia_ativo']?></td>
            <td><?php echo $not['noticia']?></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000">
                <a href="<?php echo 'alterar_noticia.php'?>">Alterar</a>
            </font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000">
                <a href="<?php echo 'op_noticia.php'?>">Excluir</a>
            </font></td>
        </tr>
        <?php
            }
        }
        ?>
    </table>
</body>
</html>