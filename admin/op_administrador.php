<?php

    require_once('../config.php');

    # --- ADICIONAR UM NOVO ADM NO BANCO ---

    if(isset($_POST['cadastro']))
    {
        $adm = new Administrador
        (
            $_POST['nome'],
            $_POST['email'],
            $_POST['login'],
            $_POST['senha']
        );

        $adm->insert();

        if($adm->getId()!=null)
        {
            header("Location:principal.php?link=9&msg=ok");
        }
    }
    
    # --- EXCLUIR OS ADM'S NO BANCO ---

    $id = filter_input(INPUT_GET,'id');
    $excluir = filter_input(INPUT_GET,'excluir'); 

    if(isset($id)&& $excluir==1)
    {
        $admin = new Administrador();
        $admin->setId($id);
        $admin->delete();
        header("Location:principal.php?link=98msg=ok");
    }

    # --- LISTAR E ATUALIZAR OS ADM'S NO BANCO ---

    if(isset($_POST['alterar']))
    {
        $adm = new Administrador();
        
        if($adm->getId()!=null)
        {
            header("Location:index.php?link=98msg=ok"); 
        }
    }

    # LOGIN DO ADMINISTRADOR

    if(isset($_POST['txt_login']))
    {
        $txt_login = isset($_POST['txt_login'])?$_POST['txt_login']:'';
        $txt_senha = isset($_POST['txt_senha'])?$_POST['txt_senha']:'';

        // PRIMIERO CASO
    
        if(empty($txt_login) || empty($txt_senha))
        {            
            header('Location: index.php?msg=Preencha o Login e Senha');
            exit;
        }
    
        $adm = new Administrador();
        $adm->efetuarlogin($txt_login,$txt_senha);
        if ($adm->getId()>0) {
            header('Location: principal.php?link=1');

        }
        elseif(($adm->getId()==null))
        {
            header('Location: index.php?msg=Login e Senha incorretos');
            exit;
        }
    
        # SESSÃO DOS USUARIOS

        $_SESSION['logado'] = true;
        $_SESSION['id_adm'] = $adm->getId();
        $_SESSION['nome_adm'] = $adm->getNome();
        $_SESSION['login_adm'] = $adm->getLogin();
        header('Location: index.php?link=');
    
        
    }
    if(isset($_GET['sair']))
        {
            $_SESSION['logado'] = null;
            $_SESSION['id_adm'] = null;
            $_SESSION['nome_adm'] = null;
            $_SESSION['login_adm'] = null;
            header('Location: index.php?link=');
        }

?>