<?php
    require_once('../config.php');
    if (isset($_POST['cadastro_usuario']))
    {
        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $foto = $_FILES['foto'];
        if(!empty($foto['name']))
        {
            $largura = 640;
            $altura = 425;
            $tamanho = 300000;
            $error = array();
            //Verificando se o arquivo é uma imagem
            if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/",$foto['type']))
            {
                $error[1] = "Este arquivo não é uma imagem";
            }
            //Verificando largura da imagem em pixels
            $dimensoes = getimagesize($foto['tmp_name']);            
            if ($dimensoes[0]>$largura)
            {
                $error[2] = 'A largura da imagem ('.$dimensoes[0].' pixels) é maior do que a suportada ( '.$largura.' pixels)';
            }
            //Verificando altura da imagens em pixels
            if ($dimensoes[1]>$altura)
            {
                $error[3] = 'A Altura da imagem ('.$dimensoes[1].' pixels) é maior do que a suportada ( '.$altura.' pixels)';
            }
            //Verificando se o tamanho em bytes da foto é mario do que o suportado
            if ($foto['size']>$tamanho)
            {
                $error[4] = 'O tamanho da imagem ( '.$foto['size'].' Bytes) é maior do que a suportada ('.$tamanho.' bytes)';
            }
            //Se não houver erros ele recupera a extensão do arquivo
            if(count($error) == 0)
            {
                //Recuperando extensão do arquivo com expressão regular
                preg_match("/\.(gif|bmp|png|jpg){1}$/i",$foto['name'],$ext);
                //Tornando nome da imagem como md5 e adicionando extensão
                $nome_img = md5(uniqid(time())).$ext[0];
                //Definindo caminho da imagem
                $caminho_img = "foto/".$nome_img;
                move_uploaded_file($foto['tmp_name'],$caminho_img);
                $user = new Usuario();
                $user->insertUser($nome,$email,$nome_img);
                header('location:principal.php?link=13');
            }
            if(count($error) != 0)
            {
                foreach ($error as $erro) 
                {
                    echo $erro.'<br>';
                }
            }
        }        
    }
?>