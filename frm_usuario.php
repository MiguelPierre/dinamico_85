<?php
     include_once('config.php');
    
    if(isset($_SESSION['logado']))
    {
        if($_SESSION['logado'])
        {
            header('location: logado.php');
        }
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
    
<head>    
    <title>Formulario de login</title>
    <link rel="stylesheet" href="../dinamico_85/css/style.css">
</head>
<body>
    <div id="box-login">
        <form action="op_usuario.php" method="POST" name="frm_usuario">
            <fieldset>
                <input type="hidden" id="id" name="id">                
                <label for="">EMAIL</label>
                <input type="text" name="txt_email" id="txt_email" required>
                <br>
                <label for="">Senha</label>
                <input type="password" name="txt_senha" id="txt_senha" required>
                <br>
                <input type="submit" name="btn_logar_user" id="logar" value="Logar" class="botao">
                
            </fieldset>
        </form>
    </div>
    
</body>
</html>