<?php
    require_once('config.php');

    if(isset($_POST['txt_email']))
    {
        $txt_email = isset($_POST['txt_email'])?$_POST['txt_email']:'';
        $txt_senha = isset($_POST['txt_senha'])?$_POST['txt_senha']:'';

        if(empty($txt_email) || empty($txt_senha))
        {
            header('location: frm_usuario.php?msg=preencha os dados de login ');
            exit;
        }

        $usr = new Usuario();
        $usr->efetuarlogin($txt_email, $txt_senha);
        if ($usr->getId()>0)
        {
            header('location: logado.php?link=');
        }
        
        elseif(($usr->getId()==null))
        {
            header('location: frm_usuario.php?msg= Email ou Senha incorretos');
            exit;
        }

        $_SESSION['logado'] = true;
        $_SESSION['id_usuario'] = $usr->getId();
        $_SESSION['nome_usuario'] = $usr->getNome();
        $_SESSION['email_usuario'] = $usr->getEmail();
        $_SESSION['senha_usuario'] = $usr->getSenha();
        header('location: logado.php?link=');
    }

        if(isset($_GET['sair']))
        {
            $_SESSION['logado'] = null;
            $_SESSION['id_usuario'] = null;
            $_SESSION['nome_usuario'] = null;
            $_SESSION['email_usuario'] = null;
            header('location: frm_usuario.php?link=');
        }

        

?>