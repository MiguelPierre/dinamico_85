<?php    
    require_once('config.php');    
    if(isset($_SESSION['logado_usuario']))
    {
        if($_SESSION['logado_usuario']==false)
        {
            header('location:index.php?link=4&msg=fazer login pra visualizar!');
            exit;
        }
    }
    else
    {
        header('location:index.php?link=4&msg= visualizar');
        exit;
    }

    $pos = new Post();    
    $idPost = $_SESSION['idpost'];    
    if(isset($idPost) && $idPost > 0)
    {
        $pos->consultarIdPost($idPost);
        if($pos->getId()>0)
        {
            $pos->updateVisita($pos->getId());
?>
<h1><?php echo $pos->getTitulo(); ?></h1><br>
<h3><?php echo $pos->getData(); ?></h3><br>
<h4><?php echo $pos->getVisitas();?></h4>
<img src="admin/foto/<?php echo $pos->getImg();?>" alt="">

<h5><?php echo $pos->getDescricao();?></h5>
<?php
        }
    }
?>